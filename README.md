# CpaUi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.3.

## Prerequsites

- Need to have `Angular Cli, Node, npm` installed in your machine.
- Need to have the API up and running at https://localhost:5001 before you start this project.

## Make it run

- Clone the repo to your local machine: `git clone https://bobvu@bitbucket.org/bobvu/cpa-ui.git`
- Open Terminal (Mac) / Cmd (Windows) at the source code folder (`cpa-ui`)
- run `npm install`
- run `ng serve --open`. Navigate to `http://localhost:4200/`
- To test: `ng test`. A new window will be shown up to display the test results.
