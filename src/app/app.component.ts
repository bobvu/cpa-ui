import { ISubject } from "./../models/ISubject";
import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ISubjectYear } from "src/models/ISubjectYear";
import { AppService } from "./app.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  title = "cpa-ui";
  results: [string, string[]][];
  constructor(private appService: AppService) {}

  ngOnInit(): void {
    this.appService.getResults().subscribe((response) => {
      this.results = this.appService.convertToResults(response);
      console.log(this.results);
    });
  }
}
