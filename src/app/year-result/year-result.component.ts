import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-year-result",
  templateUrl: "./year-result.component.html",
  styleUrls: ["./year-result.component.scss"],
})
export class YearResultComponent implements OnInit {
  @Input() result: [string, string[]];

  constructor() {}

  ngOnInit(): void {}
}
