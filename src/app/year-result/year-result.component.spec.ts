import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { YearResultComponent } from "./year-result.component";

describe("YearResultComponent", () => {
  let component: YearResultComponent;
  let fixture: ComponentFixture<YearResultComponent>;
  let h4: HTMLElement;
  let ul: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [YearResultComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YearResultComponent);
    component = fixture.componentInstance;
    component.result = ["2015", ["a", "b"]];
    h4 = fixture.nativeElement.querySelector("h4");
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should display year", () => {
    // Hooray! No `fixture.detectChanges()` needed
    expect(h4.textContent).toContain("2015");
  });
});
