import { environment } from "./../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ISubject } from "src/models/ISubject";
import { ISubjectYear } from "src/models/ISubjectYear";

@Injectable({
  providedIn: "root",
})
export class AppService {
  private baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {}

  public getResults() {
    return this.http.get<ISubject[]>(this.baseUrl + "/results");
  }

  public convertToResults(subjects: ISubject[]): [string, string[]][] {
    const sortedSubjects = subjects.sort((a, b) =>
      a.subject.localeCompare(b.subject)
    );
    let resultArray: ISubjectYear[] = [];
    sortedSubjects.map((item) => {
      item.results.map((result) => {
        if (result.grade === "PASS") {
          var a = { subject: item.subject, year: result.year };
          resultArray.push(a);
        }
      });
    });
    const subjectByYear = Object.entries(
      resultArray.reduce((obj, result) => {
        const year = result.year.toString();
        obj[year] = obj[year]
          ? [...obj[year], result.subject]
          : [result.subject];

        return obj;
      }, {} as { [key: string]: string[] })
    );

    const sortArrayAgain = subjectByYear.sort((a, b) =>
      a[0].localeCompare(b[0])
    );

    return sortArrayAgain;
  }
}
