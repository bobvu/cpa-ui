import { ISubject } from "./../models/ISubject";
import { TestBed } from "@angular/core/testing";

import { AppService } from "./app.service";
import { of } from "rxjs";
import { HttpErrorResponse } from "@angular/common/http";

describe("AppService", () => {
  let service: AppService;
  let httpClientSpy: { get: jasmine.Spy };
  const expectedResponse: ISubject[] = [
    {
      subject: "Strategic Management Accounting",
      results: [
        {
          year: 2015,
          grade: "PASS",
        },
        {
          year: 2016,
          grade: "PASS",
        },
      ],
    },
    {
      subject: "Financial Reporting",
      results: [
        {
          year: 2015,
          grade: "FAIL",
        },
        {
          year: 2016,
          grade: "PASS",
        },
      ],
    },
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({});

    httpClientSpy = jasmine.createSpyObj("HttpClient", ["get"]);
    service = new AppService(<any>httpClientSpy);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should return expected results (HttpClient called once)", () => {
    httpClientSpy.get.and.returnValue(of(expectedResponse));

    service
      .getResults()
      .subscribe(
        (results) =>
          expect(results).toEqual(expectedResponse, "expected results"),
        fail
      );
    expect(httpClientSpy.get.calls.count()).toBe(1, "one call");
  });

  it("should output years in ascending order", () => {
    let finalResults = service.convertToResults(expectedResponse);
    var year1 = finalResults[0][0];
    var year2 = finalResults[1][0];
    expect(year1).toEqual("2015");
    expect(year2).toEqual("2016");
  });

  it("should output subjects in alphabetical order", () => {
    let finalResults = service.convertToResults(expectedResponse);
    let year2 = finalResults[1][0];
    let subject1 = finalResults[1][1][0];
    let subject2 = finalResults[1][1][1];

    expect(year2).toEqual("2016");
    expect(subject1).toEqual("Financial Reporting");
    expect(subject2).toEqual("Strategic Management Accounting");
  });
});
