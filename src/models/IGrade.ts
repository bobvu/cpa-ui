export interface IGrade {
  year: number;
  grade: string;
}
