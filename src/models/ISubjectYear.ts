export interface ISubjectYear {
  year: number;
  subject: string;
}
