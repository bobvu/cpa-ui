import { IGrade } from "./IGrade";
export interface ISubject {
  subject: string;
  results: IGrade[];
}
